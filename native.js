const
    fs = require('fs'),
    _ = require('lodash');

const
    exceptions = require('./exceptions');

const
    TemplateFormatError = exceptions.TemplateFormatError,

    ValidationError = exceptions.ValidationError,
    NativeValidationError = exceptions.NativeValidationError,
    ListValidationError = exceptions.ListValidationError,
    KeysValidationError = exceptions.KeysValidationError;


class Template {

    constructor(name = 'config', strict, value) {
        this._name = name;
        this._strict = strict;
        this.value = Template.template(value, name, strict);
    }

    static template(value, name = 'config', strict) {
        if (value === undefined) {
            return Template(name, strict);
        }

        if (strict) {
            if ([Number, Boolean, String, Array, Object].includes(value)) {
                return Native(value, name, strict);
            }
        }
        else if (value.constructor === Function) {
            return Native(value, name, strict);
        }

        if (value.constructor === Object) {
            return Dict(value, name, strict);
        }

        if (value.constructor === Array) {
            return List(value, name, strict);
        }

        if ([Number, Boolean, String].includes(value.constructor)) {
            return fallback(value.constructor, value, name, strict);
        }

        if (value instanceof Template) {
            value.rebuild(name, strict);
            return value;
        }

        throw new TemplateFormatError(value);
    }

    load(filepath, full, strict) {
        // eslint-disable-next-line no-sync
        return this.output(JSON.parse(fs.readFileSync(filepath)), full, strict);
    }

    validate() { }

    output(config) {
        return config;
    }

    example() {
        return 'example';
    }

    rebuild(name, strict) {
        this._name = name;
        this._strict = strict;
    }

    get name() {
        return this._name;
    }

    set name(new_name) {
        this._name = new_name;
    }

    get strict() {
        return this._strict;
    }

    set strict(new_strict) {
        this._strict = new_strict;
    }
}

class Native extends Template {

    constructor(value, name, strict) {
        super(name, strict);
        if ([Number, Boolean, Object, Array, String].includes(value)) {
            this.value = value;
        }
        else if (!strict) {
            this.value = value;
        }
        else {
            throw new TemplateFormatError(value);
        }
    }

    validate(config, strict) {
        if ((this.strict || strict) && !(config.constructor === this.value)) {
            throw new NativeValidationError(this.value, config, this.name);
        }

        try {
            const old_type = config.constructor;
            const converted = this.value(config);
            const back_conv = old_type(converted);
            if (back_conv !== config) {
                throw new NativeValidationError(this.value, config, this.name);
            }
        }
        catch (error) {
            throw new NativeValidationError(this.value, config, this.name);
        }
    }

    example() {
        if (this.value === String) {
            return 'example';
        }
        return this.value();
    }

    output(config, full, strict) {
        if (this.strict || strict) {
            this.validate(config, true);
            return config;
        }
        else {
            this.validate(config, strict);
            return this.value(config);
        }
    }
}


class Dict extends Template {

    constructor(value, name = 'config', strict) {
        super(name, strict);
        this.value = _.mapValues(value, (v, k) => Template.template(v, `${name}[${k}]`, strict));
    }

    validate(config, strict) {
        if (!_.isObject(config)) {
            throw new NativeValidationError(Object, config, this.name);
        }

        if (this.strict || strict) {
            const keys = _.keys(_.omit(config, _.keys(this.value)));
            if (keys.length > 0) {
                throw new KeysValidationError(keys, this.name);
            }
        }
        else {
            _.forEach(this.value, (v, k) => v.validate(config[k], strict));
        }
    }

    example(full) {
        return _.pickBy(_.mapValues(this.value, (v) => v.example(full)), (v) => v !== undefined);
    }

    output(config, full, strict) {
        this.validate(config, strict);
        const keys = _.union(_.keys(config), _.keys(this.value));
        const output = {};
        _.forEach(keys, (key) => {
            const t = this.value[key];
            let v = config[key];
            if (!t && v !== undefined) {
                output[key] = v;
                return;
            }
            if (t && v === undefined) {
                v = t.example(full);
            }
            if (t && v !== undefined) {
                v = t.output(v, full, strict);
            }
            if (v !== undefined) {
                output[key] = v;
            }
        });
        return output;
    }

    rebuild(name, strict) {
        this._name = name;
        this._strict = strict;
        _.forEach(this.value, (v, k) => v.rebuild(`${name}[${k}]`, strict));
    }

    set name(new_name) {
        this._name = new_name;
        _.forEach(this.value, (v, k) => {v.name = `${new_name}[${k}]`;});
    }

    set strict(new_strict) {
        this._strict = new_strict;
        _.forEach(this.value, (v) => {v.strict = new_strict;});
    }
}


class List extends Template {

    constructor(value, name, strict) {
        super(name, strict);
        this.value = _.map(value, (v, i) => Template.template(v, `${name}[${i}]`, strict));
    }

    validate(config, strict) {
        if (!_.isArray(config)) {
            throw new NativeValidationError(Array, config, this.name);
        }
        for (const t of this.value) {
            try {
                for (const e of config) {
                    t.validate(e, strict);
                }
                return t;
            }
            catch (error) {
                if (!(error instanceof ValidationError)) {
                    throw error;
                }
            }
        }
        throw new ListValidationError(this.value, config, this.name);
    }

    example(full) {
        return [this.value[0].example(full)];
    }

    output(config, full, strict) {
        const t = this.validate(config, strict);
        return _.map(config, (e) => t.output(e, full, strict));
    }

    rebuild(name, strict) {
        this._name = name;
        this._strict = strict;
        _.forEach(this.value, (v) => v.rebuild(name, strict));
    }

    set name(new_name) {
        this._name = new_name;
        _.forEach(this.value, (v, k) => {v.name = `${new_name}[${k}]`;});
    }

    set strict(new_strict) {
        this._strict = new_strict;
        _.forEach(this.value, (v) => {v.strict = new_strict;});
    }
}


class optional extends Template {

    constructor(value, name, strict) {
        super(name, strict, value);
    }

    validate(config, strict) {
        if (config !== undefined) {
            this.value.validate(config, strict);
        }
    }

    example(full) {
        if (full) {
            return this.value.example();
        }
    }

    output(config, full, strict) {
        this.validate(config, strict);
        if (config !== undefined) {
            return this.value.output(config, full, strict);
        }
        return this.example(full);
    }
}

class fallback extends optional {

    constructor(value, default_, name, strict) {
        super(value, name, strict);
        this.default = default_;
    }

    example() {
        return this.default;
    }

    output(config, full, strict) {
        if (config === undefined) {
            return this.default;
        }
        return this.value.output(config, full, strict);
    }
}

module.exports = {Template, List, optional, fallback};
