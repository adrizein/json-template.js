const
    _ = require('lodash');

const
    template = require('./native'),
    exceptions = require('./exceptions');

const
    Template = template.Template,
    List = template.List;

const
    TemplateTypeError = exceptions.TemplateTypeError,

    ValidationError = exceptions.ValidationError,
    NativeValidationError = exceptions.NativeValidationError,
    MixinValidationError = exceptions.MixinValidationError,
    CastValidationError = exceptions.CastValidationError,
    SizeValidationError = exceptions.SizeValidationError;


class strict extends Template {

    constructor(value, name) {
        super(name, true, value);
    }

    validate(config) {
        this.value.validate(config, true);
    }

    output(config, full) {
        return this.value.output(config, full, true);
    }

    example(full) {
        return this.value.example(full);
    }

    // eslint-disable-next-line no-unused-vars
    set strict(s) { }
}


class mixin extends Template {

    constructor(...templates) {
        super();
        this.value = _.map(templates, (t) => Template.template(t));
    }

    validate(config, strict_) {
        for (const t of this.value) {
            try {
                t.validate(config, strict_);
                return t;
            }
            catch (error) {
                if (!(error instanceof ValidationError)) {
                    throw error;
                }
            }
        }
        throw new MixinValidationError(this.value, config, this.name);
    }

    example(full) {
        return this.value[0].example(full);
    }

    output(config, full, strict_) {
        const t = this.validate(config, strict_);
        return t.output(config, full, strict_);
    }

    rebuild(name, strict_) {
        this._name = name;
        this._strict = strict_;
        _.forEach(this.value, (t) => t.rebuild(name, strict_));
    }

    set name(name) {
        this._name = name;
        _.forEach(this.value, (t) => {t.name = name;});
    }

    set strict(strict_) {
        this._strict = strict_;
        _.forEach(this.value, (t) => {t.strict_ = strict_;});
    }
}


class tuple extends List {

    validate(config, strict_) {
        if (!_.isArray(config)) {
            throw new NativeValidationError(Array, config, this.name);
        }
        if (this.value.length === config.length) {
            _.zipWith(config, this.value, (e, t) => t.validate(e, strict_));
        }
        else {
            throw new SizeValidationError(this.value.length, this.value.length, config.length, this.name);
        }
    }

    example(full) {
        return _.map(this.value, (v) => v.example(full));
    }

    output(config, full, strict_) {
        this.validate(config, strict_);
        return _.zipWith(this.value, config, (v, e) => v.output(e, full, strict_));
    }
}

class cast extends Template {

    constructor(target, source, name, strict_) {
        super(name, strict_, source || mixin(Number, String, Boolean, Array, Object));
        this.target = target;
    }

    example(full) {
        try {
            return this.target(this.value.example(full));
        }
        catch (e) {
            return this.target();
        }
    }

    validate(config, strict_) {
        this.value.validate(config, strict_);
        try {
            const v = this.value.output(config, false, strict);
            this.target(v);
        }
        catch (e) {
            try {
                this.target(config);
            }
            catch (error) {
                throw new CastValidationError(this.target, this.name, error);
            }
        }
    }

    output(config, full, strict_) {
        this.validate(config, strict_);
        return this.target(self.value.output(config, full, strict_));
    }
}


class spreadcast extends cast {

    validate(config, strict_) {
        this.value.validate(config, strict_);
        try {
            this.target(...this.value.output(config, false, strict_));
        }
        catch (error) {
            throw new CastValidationError(this.target, this.name, error);
        }
    }

    example(full) {
        return this.target(...this.value.example(full));
    }

    output(config, full, strict_) {
        this.validate(config, strict_);
        return this.target(...this.value.output(config, full, strict_));
    }
}

class choice extends Template {

    constructor(...values) {
        super();
        if (_.find(values, (v) => !_.isString(v))) {
            const types = _.union([], _.map(values, (v) => typeof v).filter((t) => t === 'string'));
            throw new TemplateTypeError(`Enums can only have string values, but this one has non string values: ${types}`);
        }
        this.value = _.union([], values);
        this.upper = _.map(this.value, (v) => _.toUpper(v));
    }

    validate(config, strict_) {
        if (!_.isString(config)) {
            throw new NativeValidationError(String, config, this.name);
        }
        const error = new ValidationError(
            `${this.name} can only have the following values: ${', '.join(this.value)}. Instead, it is equal to ${config}`
        );
        if (this.strict || strict_) {
            if (!this.value.includes(config)) {
                throw error;
            }
        }
        else if (!this.upper.includes(config.toUpper())) {
            throw error;
        }
    }

    example(full) {
        return this.value[0].example(full);
    }

    output(config, full, strict_) {
        this.validate(config, strict_);
        return config;
    }
}


module.exports = {tuple, mixin, cast, strict, spreadcast, choice};
