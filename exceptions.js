
const
    _ = require('lodash');

class TemplateError extends Error {

}

class TemplateFormatError extends TemplateError {

    constructor(type) {
        const msg = `JSON values are converted to either <Array>, <String>, <Number>, or <Boolean>, <${type.name}> are not accepted.`;
        super(msg);
    }
}

class TemplateValueError extends TemplateError {

}


class TemplateTypeError extends TemplateError {

}


class ValidationError extends Error {

}


class NativeValidationError extends ValidationError {

    constructor(expected, actual, name) {
        const msg = `${name} should be a <${expected.name}> but is actually equal to ${actual} of type <${typeof actual}>`;
        super(msg);
    }
}


class ListValidationError extends ValidationError {

    constructor(possible, actual, name) {
        const msg = `The values of ${name} can all have only one of the following types: ${format(possible)}. Instead, they have mixed types: ${format(actual)}`;
        super(msg);
    }
}


class KeysValidationError extends ValidationError {

    constructor(keys, name) {
        const msg = `The following keys are not supposed to be in ${name}: ${format(keys, '"')}`;
        super(msg);
    }
}


class SizeValidationError extends ValidationError {

    constructor(min, max, actual, name) {
        let msg = `${name} should have between ${min} and ${max} elements but actually has ${actual}`;
        if (min === max) {
            msg = `${name} should have exactly ${min} elements but actually has ${actual}`;
        }
        if (max === undefined) {
            msg = `${name} should have at least ${min} elements but actually has ${actual}`;
        }
        if (min === 0 || min === undefined) {
            msg = `${name} should have at most ${max} elements but actually has ${actual}`;
        }
        super(msg);
    }
}


class MixinValidationError extends ValidationError {

    constructor(possible, actual, name) {
        const msg = `${name} can be of the following types: ${format(possible)}. Instead, it is equal to ${actual} of type ${typeof actual}`;
        super(msg);
    }
}


class CastValidationError extends ValidationError {
    constructor(target, name, error) {
        const msg = `${name} could not be cast to ${String(target)} because the following error occurred: ${error}`;
        super(msg);
    }
}


module.exports = {
    TemplateError,
    TemplateFormatError,
    TemplateValueError,
    TemplateTypeError,

    ValidationError,
    NativeValidationError,
    ListValidationError,
    KeysValidationError,
    SizeValidationError,
    MixinValidationError,
    CastValidationError,
};

////////////////


function format(types, left = '<', right = '>') {
    let r = right;
    if (['"', "'", '`'].includes(left)) {
        r = left;
    }
    return ', '.join(_.map(types, (t) => `${left}${t}${r}`));
}

