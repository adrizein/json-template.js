
const
    _ = require('lodash');

const
    template = require('./native'),
    exceptions = require('./exceptions'),
    keywords = require('./keywords');

module.exports = _.merge(
    {
        exceptions,
        template: template.Template.template,
    },
    _.pick(template, ['optional', 'fallback']),
    keywords);
